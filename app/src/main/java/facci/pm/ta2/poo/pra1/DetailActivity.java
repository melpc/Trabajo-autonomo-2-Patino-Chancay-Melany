package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ArrowKeyMovementMethod;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");





        // INICIO - CODE6
        //Pregunta 3.1
        String object_id = getIntent().getStringExtra("object_id"); // se accede al objeto object_id

        //Pregunta 3.3 y Pregunta 3.4
        DataQuery query = DataQuery.get("item");
        query.getInBackground(object_id, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if(e == null){

                    ImageView  thumbnail=  (ImageView)findViewById(R.id.thumbnail); // Casting para que thumbnail sea un objeto de tipo ImageView
                    thumbnail.setImageBitmap((Bitmap) object.get("image")); // //muestra en el ImageView la imagen guardada en la propiedad de tipo Bitmap: image del object

                    TextView price = (TextView)findViewById(R.id.price); //Casting para que price sea un objeto de tipo TextView
                    price.setText((object.get("price")+ getString(R.string.dollar))); //se concatena en Unicode el simbolo de dolar al final del precio y muestra el precio guardado en esta propiedad

                    TextView name = (TextView)findViewById(R.id.name); //Casting para que name sea un objeto de tipo TextView
                    name.setText((String)object.get("name")); //muestra en el TextView el nombre guardado en la propiedad de tipo String : name del object

                    TextView description = (TextView)(findViewById(R.id.description)); //Casting para que description sea un objeto de tipo TextView
                    description.setMovementMethod(new ScrollingMovementMethod()); //se aplica este metodo al campo de descripcion para que el texto pueda desplazrse si supera la altura del TextView
                    description.setText((String) object.get("description")); //muestra en el TextView la descripcion guardada en la propiedad de tipo String : description del object


                }else{
                    //Error
                }
            }
        });



        // FIN - CODE6

    }



}
